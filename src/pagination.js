import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';
import './pagination.css'
class Pagination extends Component {
   render() {
       return (
           <ReactPaginate className="pages" 
           previousLabel={'previous'}
            nextLabel={'next'} 
            pageCount={10} 
            onPageChange={this.props.handlePage}
            activeClassName={"active-page"}
            containerClassName="pagination">
           </ReactPaginate>
       );
   }
}
export default Pagination;
