import React, { Component } from 'react';
import Header from './components/header/header'
import Issue from './components/list/issue'
import './App.css';
import firebase from 'firebase'
import Pagination from './pagination'
import TitleContainer from './components/title/titlecontainer' 
import Fire from './config/fire';
import {issuedata,searchClick,handlefilterauthor,handleClickSorting,handleFilterLabel,handleState} from './store/action'
import {connect} from 'react-redux'
const config = {
  apiKey: "AIzaSyBvgt4sGdfSVnRVScS9IMf4tZrnIsOTVLg",
  authDomain: "first-project-8b181.firebaseapp.com",
  databaseURL: "https://first-project-8b181.firebaseio.com",
  projectId: "first-project-8b181",
  storageBucket: "first-project-8b181.appspot.com",
  messagingSenderId: "100352504557"
};
firebase.initializeApp(config);
class App extends Component {
  constructor(props){
    super(props);
    this.state={
      data:null,
      page:1,
    }
  }
  // login=()=>{
  //   const provider=new firebase.auth.GithubAuthProvider();
  // firebase.auth().signInWithPopup(provider).then(function(result) {
    
  //   var token = result.credential.accessToken;
  //   this.setState({token:token})
    
  // }).catch(function(error) {
  //   var errorCode = error.code;
  //   var errorMessage = error.message;
  //   var email = error.email;
  //   var credential = error.credential;
  // });}
  
  
onhandlePage=(e)=>{
  console.log(e.selected)
  fetch(`https://api.github.com/repos/thousif7/test-issues/issues?page=${e.selected+1}`)
    .then(response => response.json())
    .then(response => this.setState({data:response}))
}
componentDidMount= ()=>{ 
   this.props.issuedata(1)

}
  render(){
    console.log(this.props.anything+"here")
    if(this.props.anything === undefined)
    return (
      <div>LOADING</div>
    )
    else
   { return (
      <div className="App">
      <Header searching={this.props.searchClick}/>
      <TitleContainer filtering={this.props.handlefilterauthor}
      sorting={this.props.handleClickSorting} 
      filteringlabel={this.props.handleFilterLabel}
      stateShow={this.props.handleState}/>
       <Fire />  
      {this.props.anything.map((ele) =>{
       return <Issue val={ele} />
      })
      }
      <Pagination handlePage={this.onhandlePage}/>   
      </div>
    );}
  }
}
const mapStateToProps=(state)=>{
  console.log(state.data)
  return ({
    anything:state.data
  })
}
export default connect(mapStateToProps,{issuedata,searchClick,handlefilterauthor,handleClickSorting,handleFilterLabel,handleState})(App);
// searchClick=(e)=>{
  //   if(e.key === 'Enter')
  //   {
  //     if(e.target.value === null){
  //       this.setState({
  //         data:Data
  //       })
  //     }
  //       else 
  //       {
  //         let searchData=e.target.value
  //         let DataReturned=this.state.data.filter( ele => 
  //           ele.title.includes(searchData.toLowerCase())
  //         )
  //         this.setState({data:DataReturned})
  //       }
  //     }
  //   }
  //   handleClick = (e) =>{
//     let array=this.state.data;
//      console.log(e);
//      if(e === 'created_at' || e ==='updated_at')
//     {let sortedArray  = array.sort((a,b) => Moment(a[e]) - Moment(b[e]))
//     console.log(sortedArray);
//     this.setState({data:sortedArray})
//    }else 
//    if(e==='created_atrev'){
//     let sortedArray  = array.sort((a,b) => Moment(b.created_at) - Moment(a.created_at))
//     console.log(sortedArray);
//     this.setState({data:sortedArray})
//   }
//   else if(e==='updated_atrev'){
//     let sortedArray  = array.sort((a,b) => Moment(b.updated_at) - Moment(a.updated_at))
//     console.log(sortedArray);
//     this.setState({data:sortedArray})
//   }
// }
//   handleFilterLabel=(e)=>{
  //     let filteredLabel=this.state.data.filter(
  //       ele => {
  //         let temp=false;
  //         ele.labels.forEach(le=>{          
  //             if(le.name === e)
  //             temp=true
  //       })
  //     return temp;})
  //     this.setState({data:filteredLabel})
  //     console.log(e)
  //   }
  // handleFilterAuthor=(e)=>{
  //   let filterData=this.state.data.filter(ele =>
  //     e === ele.user.login
  //   )
  //   this.setState({data:filterData},()=>console.log(this.state.data))
  //   console.log(filterData); 
  // }

// handlestate=(e)=>{
//   let stateData=this.state.data.filter((ele)=>{
//     return  ele.state === e
//   })
//   this.setState({data:stateData})
// }