import Moment from 'moment'
import firebase from 'firebase'
var newToken=''
const initialState={
buttontext:'login',
data:[],
page:1,
token:'',
issue:null,
comments:[],
authors:[],
labels:[]
}

const reducer=(state=initialState,action)=>{
    if(action.type === "ISSUE_CLICK")
    {
        if(action.event === null){
            return {
                    ...state,                    
            }
        }
        else
        {
            let searchData=action.event
          let DataReturned=state.data.filter( ele => 
            ele.title.includes(searchData.toLowerCase())
          )
          return{
              ...state,
              data:DataReturned
          }
        }
    }
    if(action.type === "ISSUE_DATA")
    {
        return{
            ...state,
            data:action.payload
        }
    }
    if(action.type === "FILTER_AUTHOR")
    {
        console.log("here")
        let author=state.data.reduce((acc,val)=>{
            if(!acc.includes(val.user.login)){            
                acc.push(val.user.login)          
            }
            return acc;
        },[])
        return {
            ...state,
            authors:author
        }
    }
    if(action.type === "HANDLEFILTER_AUTHOR")
    {
        let filterData=state.data.filter(ele =>
            action.payload === ele.user.login
          )
          return {
              ...state,
              data:filterData
            }      
    }
    if(action.type === "HANDLECLICK_SORTING")
    {
        let array=[...state.data];
        let e=action.payload
        if(e === 'created_at' || e ==='updated_at')
       {let sortedArray  = array.sort((a,b) => Moment(a[e]) - Moment(b[e]))
       console.log(sortedArray);
       return {
        ...state,   
        data:sortedArray}
      }else 
      if(e==='created_atrev'){
       let sortedArray  = array.sort((a,b) => Moment(b.created_at) - Moment(a.created_at))
       console.log(sortedArray);
       return {
        ...state,   
        data:sortedArray}
     }
     else if(e==='updated_atrev'){
       let sortedArray  = array.sort((a,b) => Moment(b.updated_at) - Moment(a.updated_at))
       console.log(sortedArray);
       return {
        ...state,   
        data:sortedArray}
     }
    }
    if(action.type === "FILTER_LABEL"){   
        console.log(action.payload.labs)
        return{
            ...state,
            labels:action.payload.labs
        }
    }
    if(action.type === "HANDLEFILTER_LABEL"){
        let filteredLabel=state.data.filter(
                   ele => {
                     let temp=false;
                     ele.labels.forEach(le=>{          
                         if(le.name === action.payload)
                         temp=true
                   })
                 return temp;})
                 return{
                     ...state,
                     data:filteredLabel
                 }
    }
    if(action.type === "HANDLE_STATE"){
        
            let stateData=state.data.filter((ele)=>{
              return  ele.state === action.payload
            })
            return{
                ...state,
                data:stateData
            }
          
    }
    if(action.type === "SEND_COMMENT"){
        let comment=[...state.comments]      
                comment.push(action.payload.res)
                console.log(comment)
                return{
                    ...state,
                    comments:comment}
    }
    if(action.type === "COMMENT_DATA"){
        return{
            ...state,
            comments:action.payload
        }
    }
    if(action.type === "ISSUE_DETAILS"){
        return{
            ...state,
            issue:action.payload
        }
    }
    if(action.type === "HANDLE_DELETE"){
        let comment=[...state.comments]
        comment.map(ele => {
            if(ele.id !== action.payload){
                return ele
            }
        })
        return{
            ...state,
            comments:comment
        }
    }
    if(action.type === "HANDLE_LOGIN"){
        const provider=new firebase.auth.GithubAuthProvider();
        provider.addScope('repo');
        console.log("inside handle_login", state.buttontext)
        if( state.buttontext === 'login') {
            console.log("inside if handle_login")
      firebase.auth().signInWithPopup(provider).then(function(result) {      
      newToken = result.credential.accessToken;
      sessionStorage.setItem('data',newToken);       
      console.log(newToken)
      }).then(res =>  {
        return {
          
               ...state,
          buttontext:'logout'}
      })
    }
    else if(state.buttontext === 'logout')
     { firebase.auth().signOut().then(function() {
        sessionStorage.removeItem('data')
      }).then(res =>  {return {
        ...state,
   buttontext:'login'}})   
    }}
        
return state;
}
export default reducer;