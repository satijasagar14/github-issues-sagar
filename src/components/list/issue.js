import React, { Component } from 'react'
import './issue.css'
import Moment from 'moment'
class issue extends Component {  
  render() {
    let url=`http://localhost:3000/issues/${this.props.val.number}`
    return (
    
      <div className="issue-container">
        <div className="issue-title"><a href={url}>{this.props.val.title}</a></div>
        <div>
          {this.props.val.labels.map(ele => {
            let style1={
              backgroundColor:`#${ele.color}`,
              borderRadius:3, 
              margin:8,
            }
            return <span style={style1}>{ele.name }</span>
          })}
        </div>
        <div className="greyed-line">
        <div className="issue-id"> #{this.props.val.number}</div> &nbsp; &nbsp;
        <div className="date"> opened {Moment(this.props.val.updated_at).fromNow()}</div> &nbsp;
        <div className="user-name"> by<a href={this.props.val.user.html_url}> {this.props.val.user.login}</a></div>
        </div>
      </div>
    )
  }
}

export default issue
