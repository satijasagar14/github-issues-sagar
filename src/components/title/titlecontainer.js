import React, { Component } from 'react'
import './titlecontainer.css';
import {connect} from 'react-redux'
import {filterAuthorMethod,filterLabelMethod} from './../../store/action'

let filterauthor;
let label=[],labeloption,q=[];
class titlecontainer extends Component {
    state={
        data:this.props.data
    }
// labelFilter=()=>{
//     this.state.data.map((ele)=>{
//        ele.labels.forEach((le)=>{
//            if(!label.includes(le.name))
//         label.push(le.name)
//       })
//      })
//      return label 
//       }
componentDidMount=()=>{
    this.props.filterAuthorMethod()
    this.props.filterLabelMethod()
    console.log(this.props.author)
}
        
  render() {    
      
    //labeloption=this.labelFilter()
      if(this.props.anything === undefined || this.props.author === undefined || this.props.label === undefined)
      return <div>Loading</div>
      else
      {
    return (
      <div className="title-container">
        <div className="title">
        <select onChange={(e)=>this.props.sorting(e.target.value)}>
            <option selected>Sorting</option>
            <option value="created_at">Newest</option>
            <option value="updated_at">Latest Updated</option>
            <option value="created_atrev">Oldest</option>
            <option value="updated_atrev">Oldest Updated</option>
        </select>   
           
        <select onChange={(e)=>this.props.filtering(e.target.value)}>
            <option selected>Authors</option>
            {
                this.props.author.map(ele => {
             return  <option value={ele}>{ele}</option>
            })}
            </select>
             <select onChange={(e)=>this.props.filteringlabel(e.target.value)}>
            <option selected>Labels</option>
            {
                this.props.label.map(lab => {
                    return <option value={lab}>{lab}</option>
                })
            }
            </select>
            
            <select onChange={(e)=> this.props.stateShow(e.target.value)}>
            <option selected>State</option>
            <option value='open'>Open</option>
            <option value='closed'>Close</option>
            </select>
        </div> 
      </div>
    )}
  }
}
const mapStateToProps=(state)=>{
    console.log(state.authors)
    return ({
      anything:state.data,
      author:state.authors,
      label:state.labels
    })
  }
export default connect(mapStateToProps,{filterAuthorMethod,filterLabelMethod})(titlecontainer)
