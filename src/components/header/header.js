import React, { Component } from 'react'
import image from './../../images/giticon.png' 
import './header.css'

class header extends Component {
  render() {
    return (
      <div className="header">
        <img className="git-icon" src={image}/>
        <input className="input-search" type="text" placeholder="search" onKeyDown={(e)=> this.props.searching(e)}/>
      </div>
    )
  }
}

export default header
