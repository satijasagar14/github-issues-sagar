import React, { Component } from 'react'
import MarkDown from 'react-markdown'
import './issueBody.css'
import {connect} from 'react-redux'
import {viewComment,issueDetails,sendComment,handleDelete} from './../../store/action'
var newToken=sessionStorage.getItem('data')
console.log(newToken)
class issueBody extends Component {
    constructor(props){
        super(props);
        this.state={
          issue:null,
          comments:[] 
        }
      }
      // handledelete=(e)=>{
      //     console.log(e.target.id)
      //     fetch(` https://api.github.com/repos/thousif7/test-issues/issues/comments/${e.target.id}?access_token=${newToken}`,{
      //         method:'DELETE'
      //     }).then(res  => console.log(res))
      // }
      //  sendcomment=(e)=>{
           
      //      if(e.key==='Enter')
      //     {              
                        
      //       fetch(`https://api.github.com/repos/thousif7/test-issues/issues/${this.props.match.params.Id}/comments?access_token=${newToken}`,{
      //           method:'POST',
      //           body: JSON.stringify({"body":e.target.value})                        
      //       })
      //       .then(res => res.json()
      //       )
      //       .then(res => {
      //           let comment=this.state.comments
      //           console.log(comment)
      //           comment.push(res)
      //           console.log(comment)
      //           this.setState({comments:comment})
      //           console.log(this.state.comments)
      //       }).catch(e=>{
      //               console.log(e)
      //           })
      //           e.target.value=""
                
      //  }   }  
      sendCommentMethod=(e)=>{
        this.props.sendComment(e,this.props.match.params.Id)             
      }
      handleDeleteMethod=(e)=>{
        this.props.handleDelete(e.target.id)
      }
      componentDidMount(){          
        // fetch(`https://api.github.com/repos/thousif7/test-issues/issues/${this.props.match.params.Id}`)
        //   .then(response => response.json())
        //   .then(response => this.setState({issue:response}))
        this.props.issueDetails(this.props.match.params.Id)
      this.props.viewComment(this.props.match.params.Id)  
           
      }
  render() {
      if(this.props.issue === null || this.props.comments === undefined)
      return <div>LOADING</div>
        else{   
      return (
      <div>
          <div><h2>{this.props.issue.title}</h2></div>
          <div className="user">
          <img className='avatar-image' src={this.props.issue.user.avatar_url}></img>
          <div className='user-body'><MarkDown skipHtml={true}>{this.props.issue.body}</MarkDown></div> 
          <div className="comments-container">{
              this.props.comments.map(ele =>  {       
                  return   <div className="comments">
                  <img className="avatar-image-comment"src={ele.user.avatar_url}></img>
                      <div><strong>{ele.user.login} : </strong></div>
                      <div><MarkDown skipHtml={true}>{ele.body}</MarkDown></div>
                      <button id={ele.id} onClick={this.handleDeleteMethod}>Delete</button>
            </div> 
              }   )} </div>   
          
          </div>
          <input className="comment-type" type='text' placeholder='addcomment' onKeyDown={this.sendCommentMethod}></input>
      </div>
    )
      }
  }
}
const mapStateToProps=(state)=>{
  console.log(state.comments)
  return ({
    comments:state.comments,
    issue:state.issue
  })
}
export default connect(mapStateToProps,{viewComment,issueDetails,sendComment,handleDelete})(issueBody)
