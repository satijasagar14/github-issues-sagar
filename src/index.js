import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App'
import Issue from './components/issueBody/issueBody';
import { Route, Link ,BrowserRouter,Switch } from 'react-router-dom'
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux'
import reducer from './store/reducer'
import thunk from 'redux-thunk'
let middleWare= [thunk]

const store=createStore(reducer,applyMiddleware(...middleWare))

const routing = (
    <Provider store={store}>
<BrowserRouter>
<div>  
    <Switch>    
    <Route path="/" component={App} exact></Route>
    <Route path="/issues/:Id" component={Issue} exact></Route>
    </Switch>
</div>
</BrowserRouter>
</Provider>
)
ReactDOM.render(routing, document.getElementById('root'));

