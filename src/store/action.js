var newToken=sessionStorage.getItem('data')
export const searchClick=(e)=>dispatch =>{
    if(e.key === 'Enter')
    {   
          dispatch({
            type:"ISSUE_CLICK",
            event:e.target.value
        })           
        
    }
}
export const issuedata=(page)=>dispatch=>{
    fetch(`https://api.github.com/repos/thousif7/test-issues/issues?page=${page}`)
    .then(response => response.json())
    .then(response =>  dispatch({
        type:"ISSUE_DATA",
        payload:response
    })           
)}
export const filterAuthorMethod=()=> dispatch=>{
    dispatch({
        type:"FILTER_AUTHOR"
    })
};  
export const handlefilterauthor=(e)=>dispatch=>{
    dispatch({
        type:"HANDLEFILTER_AUTHOR",
         payload:e
    })
}
export const handleClickSorting=(e)=>dispatch=>{
    dispatch({
        type:"HANDLECLICK_SORTING",
        payload:e
    })
}
export const filterLabelMethod=(e)=>dispatch=>{
    fetch(`https://api.github.com/repos/thousif7/test-issues/labels`)
    .then(response =>response.json())
    .then(response=> {
            let labs=response.map(lab => lab.name)
            dispatch({
                type:"FILTER_LABEL",
                payload:{e,labs}
            })
    })
}    
export const handleFilterLabel=(e)=>dispatch=>{
    dispatch({
        type:"HANDLEFILTER_LABEL",
         payload:e
    })
}    
export const handleState=(e)=>dispatch=>{
    dispatch({
        type:"HANDLE_STATE",
        payload:e
    })
}
export const sendComment=(e,id)=>dispatch=>{
    if(e.key === 'Enter')
    {     fetch(`https://api.github.com/repos/thousif7/test-issues/issues/${id}/comments?access_token=${newToken}`,{
        method:'POST',
        body: JSON.stringify({"body":e.target.value})                        
    })
    .then(res => res.json())
    .then(res =>   dispatch({
            type:"SEND_COMMENT",
            payload:{e,res}
        }) )          
        
    }
}
export const viewComment=(id)=>dispatch=>{
    fetch(`https://api.github.com/repos/thousif7/test-issues/issues/${id}/comments`)
          .then(res => res.json())
          .then(res => dispatch({
              type:"COMMENT_DATA",
              payload:res
          })
          )          
}
export const issueDetails=(id)=>dispatch=>{
    fetch(`https://api.github.com/repos/thousif7/test-issues/issues/${id}`)
    .then(response => response.json())
    .then(res => dispatch({
        type:"ISSUE_DETAILS",
        payload:res
    }))
}
export const handleDelete=(id)=>dispatch=>{
    fetch(` https://api.github.com/repos/thousif7/test-issues/issues/comments/${id}?access_token=${newToken}`,{
        method:'DELETE'
    }).then(res  => console.log(res))
    .then(res => dispatch({
        type:"HANDLE_DELETE",
        payload:id
    }))
}
export const login=()=>dispatch=>{
dispatch({
    type:"HANDLE_LOGIN"
})
}